// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

// Для работы с полем <input> не рекомендуется использовать клавиатурные события, потому-что в поле <input> можно ввести данные не только с клавиатуры, но и, например, с помощью мыши(копировать-вставить), или с помощью распознавания речи и т.д.



const btn = document.querySelectorAll('.btn');

document.addEventListener('keydown', e => {

    btn.forEach(elem => {
        elem.classList.remove('active');
        if (e.key.toUpperCase() === elem.innerText.toUpperCase()) {
            elem.classList.add('active');
        }
    })
})
